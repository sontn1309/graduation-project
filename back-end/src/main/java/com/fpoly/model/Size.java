package com.fpoly.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "size")
public class Size {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "size_name")
	private String sizeName;

	public int getId() {
		return id;
	}

	public Size setId(int id) {
		this.id = id;
		return this;
	}

	public String getSizeName() {
		return sizeName;
	}

	public Size setSizeName(String sizeName) {
		this.sizeName = sizeName;
		return this;
	}
	
}
