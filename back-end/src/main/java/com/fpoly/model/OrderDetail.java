package com.fpoly.model;

import javax.persistence.*;

import com.fpoly.model.key.OrderDetailKey;

@Entity
@Table(name = "order_detail")
public class OrderDetail {

	@EmbeddedId
	private OrderDetailKey orderDetailKey;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "total")
	private int total;

	public OrderDetail() {
	}

	public OrderDetail(OrderDetailKey orderDetailKey, int quantity, int total) {
		this.orderDetailKey = orderDetailKey;
		this.quantity = quantity;
		this.total = total;
	}


	/**
	 * @return the orderDetailKey
	 */
	public OrderDetailKey getOrderDetailKey() {
		return orderDetailKey;
	}

	/**
	 * @param orderDetailKey the orderDetailKey to set
	 */
	public OrderDetail setOrderDetailKey(OrderDetailKey orderDetailKey) {
		this.orderDetailKey = orderDetailKey;
		return this;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public OrderDetail setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public OrderDetail setTotal(int total) {
		this.total = total;
		return this;
	}

	
}
