package com.fpoly.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author sontn
 *
 */
@Entity
@Table(name = "model_type")
public class ModelType {
	/**
	 * 
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	/**
	 * 
	 */
	@Column(name = "model_name")
	private String modelName;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public ModelType setId(int id) {
		this.id = id;
		return this;
	}
	/**
	 * @return the modelName
	 */
	public String getModelName() {
		return modelName;
	}
	/**
	 * @param modelName the modelName to set
	 */
	public ModelType setModelName(String modelName) {
		this.modelName = modelName;
		return this;
	}

	
}
