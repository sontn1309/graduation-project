package com.fpoly.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UserApplication {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "phone")
	private String phone;
	@Column(name = "name")
	private String name;
	@Column(name = "password")
	private String password;
	@Column(name = "start_date")
	private Timestamp startDate = new Timestamp(System.currentTimeMillis());
	@Column(name = "email")
	private String email;
	@Column(name = "address")
	private String address;
	@Column(name = "confirm")
	private boolean confirm = false;
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE })
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();

	public String getPhone() {
		return this.phone;
	}

	public UserApplication setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getName() {
		return this.name;
	}

	public UserApplication setName(String name) {
		this.name = name;
		return this;
	}

	public String getPassword() {
		return this.password;
	}

	public UserApplication setPassword(String password) {
		this.password = password;
		return this;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public UserApplication setStartDate(Timestamp startDate) {
		this.startDate = startDate;
		return this;
	}

	public String getEmail() {
		return this.email;
	}

	public UserApplication setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getAddress() {
		return this.address;
	}

	public UserApplication setAddress(String address) {
		this.address = address;
		return this;
	}

	public boolean isConfirm() {
		return this.confirm;
	}

	public boolean getConfirm() {
		return this.confirm;
	}

	public UserApplication setConfirm(boolean confirm) {
		this.confirm = confirm;
		return this;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public UserApplication setRoles(Set<Role> roles) {
		this.roles = roles;
		return this;
	}

	public UserApplication addRole(Role role) {
		roles.add(role);
		role.getUser().add(this);
		return this;
	}

	public UserApplication removeRole(Role role) {
		roles.remove(role);
		role.getUser().remove(this);
		return this;
	}

	public UserApplication() {
	}

	public int getId() {
		return id;
	}

	public UserApplication setId(int id) {
		this.id = id;
		return this;
	}

	public UserApplication(int id, String phone, String name, String password, Timestamp startDate, 
			String email, String address, boolean confirm, Set<Role> roles) {
		this.id = id;
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.startDate = startDate;
		this.email = email;
		this.address = address;
		this.confirm = confirm;
		this.roles = roles;
	}

	public UserApplication(String phone, String name, String password, Timestamp startDate,  String email,
			String address, boolean confirm,  Set<Role> roles) {
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.startDate = startDate;
		this.email = email;
		this.address = address;
		this.confirm = confirm;
		this.roles = roles;
	}

	public UserApplication(String phone, String name, String password, Timestamp startDate, String email,
			String address, boolean confirm) {
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.startDate = startDate;
		this.email = email;
		this.address = address;
		this.confirm = confirm;
	}

	public UserApplication(String phone, String name, String password, String otp, String email, String address,
			boolean confirm, String jwt) {
		this.phone = phone;
		this.name = name;
		this.password = password;
		this.email = email;
		this.address = address;
		this.confirm = confirm;
	}

}