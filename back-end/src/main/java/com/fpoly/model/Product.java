package com.fpoly.model;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "product_name")
	private String name;
	
	@Column(name = "picture1")
	private String picture1;
	
	@Column(name = "picture2")
	private String picture2;
	
	@Column(name = "picture3")
	private String picture3;
	
	@Column(name = "picture4")
	private String picture4;
	
	@Column(name = "picture5")
	private String picture5;
	
	@Column(name = "price")
	private int price;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "category_id")
	private int category;
	
	@Column(name = "gender_id")
	private int genderId;

	@Column(name = "type_id")
	private int typeId;
	
	public Product() {
	}

	public int getId() {
		return id;
	}

	public Product setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Product setName(String name) {
		this.name = name;
		return this;
	}

	public String getPicture1() {
		return picture1;
	}

	public Product setPicture1(String picture1) {
		this.picture1 = picture1;
		return this;
	}

	public String getPicture2() {
		return picture2;
	}

	public Product setPicture2(String picture2) {
		this.picture2 = picture2;
		return this;
	}

	public String getPicture3() {
		return picture3;
	}

	public Product setPicture3(String picture3) {
		this.picture3 = picture3;
		return this;
	}

	public String getPicture4() {
		return picture4;
	}

	public Product setPicture4(String picture4) {
		this.picture4 = picture4;
		return this;
	}

	public String getPicture5() {
		return picture5;
	}

	public Product setPicture5(String picture5) {
		this.picture5 = picture5;
		return this;
	}

	public int getPrice() {
		return price;
	}

	public Product setPrice(int price) {
		this.price = price;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Product setDescription(String description) {
		this.description = description;
		return this;
	}

	public int getQuantity() {
		return quantity;
	}

	public Product setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}

	public int getCategory() {
		return category;
	}

	public Product setCategory(int category) {
		this.category = category;
		return this;
	}

	public int getGenderId() {
		return genderId;
	}

	public Product setGenderId(int genderId) {
		this.genderId = genderId;
		return this;
	}

	public int getTypeId() {
		return typeId;
	}

	public Product setTypeId(int typeId) {
		this.typeId = typeId;
		return this;
	}
	
	
}
