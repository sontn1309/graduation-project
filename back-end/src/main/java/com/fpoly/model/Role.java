package com.fpoly.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name="name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,
    cascade = {  CascadeType.ALL
        },mappedBy = "roles")
    @JsonIgnore
    private Set<UserApplication> user=new HashSet<>();

    public Role(int id, String name, Set<UserApplication> user) {
        this.id = id;
        this.name = name;
        this.user = user;
    }

    public Role(String name, Set<UserApplication> user) {
        this.name = name;
        this.user = user;
    }

    public Role() {
    }

    public int getId() {
        return this.id;
    }

    public Role setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public Set<UserApplication> getUser() {
        return this.user;
    }

    public Role setUser(Set<UserApplication> user) {
        this.user = user;
        return this;
    }
}