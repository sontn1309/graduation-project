package com.fpoly.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fpoly.model.key.RepoKey;

@Entity
@Table(name = "repo")
public class Repo {
	
	@EmbeddedId
	private RepoKey repoKey;
	
	@Column(name = "total")
	private int total;
	
	@Column(name = "update_time")
	private Timestamp updateTime;

	
	public Repo(RepoKey repoKey, int total, Timestamp updateTime) {
		this.repoKey = repoKey;
		this.total = total;
		this.updateTime = updateTime;
	}

	/**
	 * @return the repoKey
	 */
	public RepoKey getRepoKey() {
		return repoKey;
	}

	/**
	 * @param repoKey the repoKey to set
	 */
	public Repo setRepoKey(RepoKey repoKey) {
		this.repoKey = repoKey;
		return this;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public Repo setTotal(int total) {
		this.total = total;
		return this;
	}

	/**
	 * @return the updateTime
	 */
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	/**
	 * @param updateTime the updateTime to set
	 */
	public Repo setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
		return this;
	}
	
	


	
}
