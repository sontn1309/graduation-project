package com.fpoly.model;

import javax.persistence.*;

@Entity
@Table(name = "gender")
public class Gender {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "gender_name")
	private String genderName;

	public Gender() {
	}

	public int getId() {
		return id;
	}

	public Gender setId(int id) {
		this.id = id;
		return this;
	}

	public String getGenderName() {
		return genderName;
	}

	public Gender setGenderName(String genderName) {
		this.genderName = genderName;
		return this;
	}
}
