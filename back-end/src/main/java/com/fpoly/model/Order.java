package com.fpoly.model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Column(name = "user_id")
    private int userId;
    
    @Column(name = "address")
    private String address;
    
    @Column(name = "phone_number")
    private String phoneNumber;
    
    @Column(name = "total")
    private int total;
    
    @Column(name = "date_create")
    private Date dateCreate;
    
    @Column(name = "status_id")
    private int statusId;
    
    @Column(name = "date_end")
    private Date dateEnd;
    
    @Column(name="shipper_id")
    private int shipperId;

    public Order() {
    }

    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Order setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Order setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Order setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public int getTotal() {
        return total;
    }

    public Order setTotal(int total) {
        this.total = total;
        return this;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public Order setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
        return this;
    }

    public int getStatusId() {
        return statusId;
    }

    public Order setStatusId(int statusId) {
        this.statusId = statusId;
        return this;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public Order setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
        return this;
    }

	/**
	 * @return the shipperId
	 */
	public int getShipperId() {
		return shipperId;
	}

	/**
	 * @param shipperId the shipperId to set
	 */
	public Order setShipperId(int shipperId) {
		this.shipperId = shipperId;
		return this;
	}
    
}
