package com.fpoly.model.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderDetailKey implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "product_id")
	private int productId;
	
	@Column(name = "order_id")
	private int orderId;

	
	
	public OrderDetailKey() {
	}

	public OrderDetailKey(int productId, int orderId) {
		this.productId = productId;
		this.orderId = orderId;
	}

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public OrderDetailKey setProductId(int productId) {
		this.productId = productId;
		return this;
	}

	/**
	 * @return the orderId
	 */
	public int getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public OrderDetailKey setOrderId(int orderId) {
		this.orderId = orderId;
		return this;
	}

	
	
	
}
