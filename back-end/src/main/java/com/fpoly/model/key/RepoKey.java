package com.fpoly.model.key;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RepoKey implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "product_id")
	private int productId;
	
	@Column(name = "size_id")
	private int sizeId;

	public RepoKey(int productId, int sizeId) {
		this.productId = productId;
		this.sizeId = sizeId;
	}

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public RepoKey setProductId(int productId) {
		this.productId = productId;
		return this;
	}

	/**
	 * @return the sizeId
	 */
	public int getSizeId() {
		return sizeId;
	}

	/**
	 * @param sizeId the sizeId to set
	 */
	public RepoKey setSizeId(int sizeId) {
		this.sizeId = sizeId;
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(productId, sizeId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RepoKey)) {
			return false;
		}
		RepoKey other = (RepoKey) obj;
		return productId == other.productId && sizeId == other.sizeId;
	}
	
	
}
