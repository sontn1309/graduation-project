package com.fpoly.model;

import javax.persistence.*;

/**
 * @author sontn
 *
 */
@Entity
@Table(name = "category")
public class Category {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "category_name")
	private String name;

	public Category() {
	}

	public int getId() {
		return id;
	}

	public Category setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Category setName(String name) {
		this.name = name;
		return this;
	}
}
