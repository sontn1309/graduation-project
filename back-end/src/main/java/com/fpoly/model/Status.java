package com.fpoly.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "status")
public class Status {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "status_name")
	private String statusName;

	public int getId() {
		return id;
	}

	public Status setId(int id) {
		this.id = id;
		return this;
	}

	public String getStatusName() {
		return statusName;
	}

	public Status setStatusName(String statusName) {
		this.statusName = statusName;
		return this;
	}
	
	
}
