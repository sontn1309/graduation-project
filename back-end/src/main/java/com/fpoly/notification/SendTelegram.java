package com.fpoly.notification;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class SendTelegram {

	public void sendTelegram() {
		try {
			Telegram telegram = new Telegram();
			RestTemplate rt = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent", "");
			HttpEntity<Telegram> entity = new HttpEntity<>(telegram, headers);
			String url = "https://toto.binstory.org/sandbox/sendMessage";
			ResponseEntity<String> res = rt.exchange(url, HttpMethod.POST, entity, String.class);
			System.out.println(res.getStatusCodeValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
