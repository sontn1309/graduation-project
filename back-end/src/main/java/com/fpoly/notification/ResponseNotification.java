package com.fpoly.notification;

import java.sql.Timestamp;

public class ResponseNotification {

   private Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
   private int status;
   private String message;

   public ResponseNotification(int status,String message) {

       this.status = status;
       this.message = message;
   }

   public Timestamp getTimeStamp() {
       return this.timeStamp;
   }

   public void setTimeStamp(Timestamp timeStamp) {
       this.timeStamp = timeStamp;
   }

   public int getStatus() {
       return this.status;
   }

   public void setStatus(int status) {
       this.status = status;
   }

   public String getMessage() {
       return this.message;
   }

   public void setMessage(String message) {
       this.message = message;
   }

}
