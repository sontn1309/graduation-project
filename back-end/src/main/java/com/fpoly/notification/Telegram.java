package com.fpoly.notification;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Telegram {
	 @JsonProperty("bot_token")
	private String botToken = "1242861915:AAFoPhbiqhXWG8S-NBEQlUbggtbjvhVZ88k";
	 @JsonProperty("chat_id")
	private int chatId = -256675704;
	 @JsonProperty("text")
	private String text = "This is a sample message";
	 @JsonProperty("disable_notification")
	private boolean disableNotification = false;
	 @JsonProperty("parse_mode")
	private String parseMode ="";
	/**
	 * @return the botToken
	 */
	public String getBotToken() {
		return botToken;
	}
	/**
	 * @param botToken the botToken to set
	 */
	public Telegram setBotToken(String botToken) {
		this.botToken = botToken;
		return this;
	}
	/**
	 * @return the chatId
	 */
	public int getChatId() {
		return chatId;
	}
	/**
	 * @param chatId the chatId to set
	 */
	public Telegram setChatId(int chatId) {
		this.chatId = chatId;
		return this;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public Telegram setText(String text) {
		this.text = text;
		return this;
	}
	/**
	 * @return the disableNotification
	 */
	public boolean isDisableNotification() {
		return disableNotification;
	}
	/**
	 * @param disableNotification the disableNotification to set
	 */
	public Telegram setDisableNotification(boolean disableNotification) {
		this.disableNotification = disableNotification;
		return this;
	}
	/**
	 * @return the parseMode
	 */
	public String getParseMode() {
		return parseMode;
	}
	/**
	 * @param parseMode the parseMode to set
	 */
	public Telegram setParseMode(String parseMode) {
		this.parseMode = parseMode;
		return this;
	}
	@Override
	public String toString() {
		return "Telegram [botToken=" + botToken + ", chatId=" + chatId + ", text=" + text + ", disableNotification="
				+ disableNotification + ", parseMode=" + parseMode + "]";
	}
	
	
	
}
