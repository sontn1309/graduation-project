package com.fpoly.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SmsSender {
        @Value("${ACCOUNT_SID}")
        public String ACCOUNT_SID ;
        @Value("${AUTH_TOKEN}")
         public String AUTH_TOKEN ;
         @Value("${PhoneSystem}")
        public String phoneToSend;
         public void sendSms( String phoneToReceive,String messageSms) {
                        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

                        Message message = Message
                                .creator(new PhoneNumber("+84"+phoneToReceive.substring(1)), 
                                        new PhoneNumber(phoneToSend), 
                                        messageSms)
                                .create();
                                message.getSid();
                }
}