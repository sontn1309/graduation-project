package com.fpoly.utility;

public class ConverterUtils {
	public static String convertPhoneNumber(String phone)
	{	String phoneNumber = phone;
		if(phone.startsWith("+"))
		{
			phoneNumber = "0"+phone.substring(3);
		}
		if (phone.startsWith("84")) {
			phoneNumber = "0"+phone.substring(2);
		}
		return phoneNumber;
	}
	
}
