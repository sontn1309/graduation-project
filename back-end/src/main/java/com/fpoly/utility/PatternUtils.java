package com.fpoly.utility;

public class PatternUtils {
	public static final String PATTERN_PHONE="^\\d{10}$";
	public static final String EMAIL_VERIFICATION = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
}
