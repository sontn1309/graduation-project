package com.fpoly.utility;

import java.util.Random;

public class NumberUtils {

    public static String generateRandomString(int length,String type)
    {   
        int leftLimit ; // letter 'a'
        int rightLimit ; // letter 'z'
        if (type.equalsIgnoreCase("UPPER")) {
            leftLimit   = 65;
            rightLimit =    90;
        } else {
            leftLimit   = 97;
            rightLimit  = 122;
        }
        int targetStringLength = length;
        Random random = new Random();
     
        String generatedString = random.ints(leftLimit, rightLimit + 1)
          .limit(targetStringLength)
          .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
          .toString();
     return generatedString;
    }
}