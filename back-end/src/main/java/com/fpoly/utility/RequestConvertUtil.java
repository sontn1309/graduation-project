package com.fpoly.utility;

import javax.servlet.http.HttpServletRequest;

import com.fpoly.jwt.JwtUtil;

public class RequestConvertUtil {
	
	public static String generatePhoneNumber(HttpServletRequest request) {
		 String authorizationHeader = request.getHeader("Authorization");
		 JwtUtil jwtTokenUtil = new JwtUtil();
		String username = null;
		String jwt = null;

		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			jwt = authorizationHeader.substring(7);
			username = jwtTokenUtil.extractUsername(jwt);
		}
		return username;
	}
}
