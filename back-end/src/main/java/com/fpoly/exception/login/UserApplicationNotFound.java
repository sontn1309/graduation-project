package com.fpoly.exception.login;

import com.fpoly.exception.TomShopException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class UserApplicationNotFound extends TomShopException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public UserApplicationNotFound(String message) {
        super(404, message);
    }

    public UserApplicationNotFound(int code, String message) {
        super(code, message);
    }
}
