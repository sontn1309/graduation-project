package com.fpoly.exception;

public class TomShopException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final int code;
    private final String message;

    protected TomShopException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }
}
