package com.fpoly.exception.response;

import java.sql.Timestamp;

public class ResponseException {
	/**
	 *
	 */
	private Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
	private int status;
	private String error;
	private String message;

	public ResponseException(int status, String error, String message) {

		this.status = status;
		this.error = error;
		this.message = message;
	}

	public Timestamp getTimeStamp() {
		return this.timeStamp;
	}

	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
