package com.fpoly.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class LoginExceptionHandle extends TomShopException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public LoginExceptionHandle(String message) {
        super(404, message);
    }

    public LoginExceptionHandle(int code, String message) {
        super(code, message);
    }
}
