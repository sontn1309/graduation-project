package com.fpoly.dto;

public class RoleDTO {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public RoleDTO setId(int id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public RoleDTO setName(String name) {
		this.name = name;
		return this;
	}

	public RoleDTO(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public RoleDTO() {
	}

	public RoleDTO(String name) {
		this.name = name;
	}

	
}
