package com.fpoly.dto.convert;

import java.util.Set;

import com.fpoly.dto.UserApplicationDTO;
import com.fpoly.model.Role;
import com.fpoly.model.UserApplication;

public class ConvertUserApplicationDTO {
	public static UserApplicationDTO conveUserApplicationToDTO(UserApplication user) {
		return new UserApplicationDTO().setAddress(user.getAddress()).setEmail(user.getEmail())
				.setName(user.getName()).setPassword(null).setPhone(user.getPhone());
	}

	public static UserApplication conveDTOToUserApplication(UserApplicationDTO user, Set<Role> role) {
		return new UserApplication().setId(user.getId()).setAddress(user.getAddress()).setEmail(user.getEmail())
				.setName(user.getName()).setPassword(user.getPassword()).setPhone(user.getPhone()).setRoles(role);
	}
}