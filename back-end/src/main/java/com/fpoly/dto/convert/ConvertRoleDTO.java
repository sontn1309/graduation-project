package com.fpoly.dto.convert;

import java.util.Set;

import com.fpoly.dto.RoleDTO;
import com.fpoly.model.Role;
import com.fpoly.model.UserApplication;

public class ConvertRoleDTO {
	public RoleDTO conveRoleToDTO(Role role) {
		return new RoleDTO().setId(role.getId()).setName(role.getName());
	}

	public Role conveDTOToRole(RoleDTO role, Set<UserApplication> user) {
		return new Role().setId(role.getId()).setName(role.getName()).setUser(user);
	}
}
