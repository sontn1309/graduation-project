package com.fpoly.dto;

public class UserApplicationDTO {
	private int id = 0;
	private String name;
	private String phone;
	private String address;
	private String password;
	private String email;

	public int getId() {
		return this.id;
	}

	public UserApplicationDTO setId(int id) {
		this.id = id;
		return this;
	}

	public String getPhone() {
		return this.phone;
	}

	public UserApplicationDTO setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getPassword() {
		return this.password;
	}

	public UserApplicationDTO setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getName() {
		return name;
	}

	public UserApplicationDTO setName(String name) {
		this.name = name;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public UserApplicationDTO setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public UserApplicationDTO setEmail(String email) {
		this.email = email;
		return this;
	}

	public UserApplicationDTO(String phone, String password) {
		this.phone = phone;
		this.password = password;
	}

	public UserApplicationDTO() {
	}

	public UserApplicationDTO(String phone) {
		this.phone = phone;
	}

	public UserApplicationDTO(String name, String phone, String address, String password, String email) {
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.password = password;
		this.email = email;
	}

}