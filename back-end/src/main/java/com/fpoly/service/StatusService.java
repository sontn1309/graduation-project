package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Status;

public interface StatusService {
	
	List<Status> Status();

	Optional<Status> findById(int id);

	Status saveStatus(Status status);

	void deleteStatus(int id);
}
