package com.fpoly.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fpoly.dto.UserApplicationDTO;
import com.fpoly.model.UserApplication;

@Service
public class MyUserDetails implements UserDetailsService {
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        Optional<UserApplication> user = userService.getUserByPhone(phoneNumber);
        if (!user.isPresent()) {
            return null;
        }

        return new User(user.get().getPhone(), user.get().getPassword(), getAuthorities(user.get()));
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(UserApplication user) {
        String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }

    public boolean checkUser(String phoneNumber, UserApplicationDTO user) throws Exception {
        Optional<UserApplication> userlogin = userService.getUserByPhone(phoneNumber);

        boolean checkPass = bCryptPasswordEncoder.matches(user.getPassword(), userlogin.get().getPassword());
        if (userlogin.get().getPhone().equalsIgnoreCase(user.getPhone()) && checkPass) {
            return true;
        }
        return false;
    }

}