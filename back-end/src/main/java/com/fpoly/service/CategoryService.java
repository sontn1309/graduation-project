package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Category;

public interface CategoryService {
	List<Category> findAllCategories();

	Optional<Category> findById(int id);

	Category saveCategory(Category caetgory);

	void deleteCategory(int id);
}
