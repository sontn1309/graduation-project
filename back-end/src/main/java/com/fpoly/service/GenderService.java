package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Gender;

public interface GenderService {
	List<Gender> findAllGenders();

	Optional<Gender> findById(int id);

	Gender saveCategory(Gender gender);

	void deleteGender(int id);
}
