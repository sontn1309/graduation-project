package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Size;

public interface SizeService {
	List<Size> findAllSizes();

	Optional<Size> findById(int id);

	Size saveSize(Size size);

	void deleteSize(int id);
}
