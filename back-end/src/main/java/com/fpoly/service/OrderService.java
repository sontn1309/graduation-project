package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Order;

public interface OrderService {
	List<Order> findAllOrders();

	Optional<Order> findById(int id);

	Order saveOrder(Order order);

	void deleteOrder(int id);
	
	List<Order> findOrdersByUserId(int UserId);
	
	Order findOrdersByUserIdAndId(int id, int userId);
}
