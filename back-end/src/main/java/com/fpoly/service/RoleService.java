package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Role;

public interface RoleService {
	Optional<Role> getRole(String id);

	List<Role> getAllRoles();

	Role saveRole(Role role);

	void deleteRole(String id);
}