package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.model.Repo;
import com.fpoly.model.key.RepoKey;
import com.fpoly.repository.RepoRepository;
import com.fpoly.service.RepoService;

@Service
public class RepoServiceImpl implements RepoService{
	
	@Autowired
	private RepoRepository repoRepository;

	@Override
	public List<Repo> findAllRepos() {
		return repoRepository.findAll();
	}

	@Override
	public Optional<Repo> findById(RepoKey repoKey) {
		return repoRepository.findById(repoKey);
	}

	@Override
	public Repo saveRepo(Repo repo) {
		return repoRepository.save(repo);
	}

	@Override
	public void deleteRepo(RepoKey repoKey) {
		repoRepository.deleteById(repoKey);
	}

	@Override
	public List<Repo> findByProductId(int productId) {
		return repoRepository.findByRepoKeyProductId(productId);
	}

	@Override
	public void deletRepoByProductId(int productId) {
		repoRepository.deleteByRepoKeyProductId(productId);
	}


}
