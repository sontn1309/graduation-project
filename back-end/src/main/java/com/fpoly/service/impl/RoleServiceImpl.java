package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Role;
import com.fpoly.repository.RoleRepository;
import com.fpoly.service.RoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Override
    public Optional<Role> getRole(String id) {
        return roleRepository.findById(Integer.parseInt(id));
    }

    @Override
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

    @Override
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public void deleteRole(String id) {
      roleRepository.deleteById(Integer.parseInt(id));

    }

}