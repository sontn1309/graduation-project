package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.model.Gender;
import com.fpoly.repository.GenderRepository;
import com.fpoly.service.GenderService;

@Service
public class GenderServiceImpl implements GenderService {
	@Autowired
	private GenderRepository genderRepository;

	@Override
	public List<Gender> findAllGenders() {
		return genderRepository.findAll();
	}

	@Override
	public Optional<Gender> findById(int id) {
		return genderRepository.findById(id);
	}

	@Override
	public Gender saveCategory(Gender gender) {
		return genderRepository.save(gender);
	}

	@Override
	public void deleteGender(int id) {
		genderRepository.deleteById(id);
	}

}
