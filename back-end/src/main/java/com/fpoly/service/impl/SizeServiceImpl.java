package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.model.Size;
import com.fpoly.repository.SizeRepository;
import com.fpoly.service.SizeService;

@Service
public class SizeServiceImpl implements SizeService {

	@Autowired
	private SizeRepository sizeRepository;

	public List<Size> findAllSizes() {
		return sizeRepository.findAll();
	}

	public Optional<Size> findById(int id) {
		return sizeRepository.findById(id);
	}

	public Size saveSize(Size size) {
		return sizeRepository.save(size);
	}

	public void deleteSize(int id) {
		sizeRepository.deleteById(id);

	}

}
