package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.UserApplication;
import com.fpoly.repository.UserRepository;
import com.fpoly.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<UserApplication> getUser(String id) {

        return userRepository.findById(Integer.parseInt(id));
    }

    @Override
    public List<UserApplication> getAllUsers() {

        return userRepository.findAll();
    }

    @Override
    public UserApplication saveUser(UserApplication user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(Integer.parseInt(id));

    }

    @Override
    public Optional<UserApplication> getUserByPhone(String phoneNumber) {
        UserApplication theTemp = null;
        try {
            List<UserApplication> listUserByPhone = userRepository.findByPhone(phoneNumber);

            if (listUserByPhone.size() > 0) {

                theTemp = listUserByPhone.get(0);
                return Optional.of(theTemp);
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            return Optional.empty();
        }

    }

}