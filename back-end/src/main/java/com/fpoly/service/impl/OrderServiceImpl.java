package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.model.Order;
import com.fpoly.repository.OrderRepository;
import com.fpoly.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	private OrderRepository orderRepository;
	

	@Override
	public List<Order> findAllOrders() {
		return orderRepository.findAll();
	}

	@Override
	public Order saveOrder(Order order) {
		return orderRepository.save(order);
	}

	@Override
	public void deleteOrder(int id) {
		orderRepository.deleteById(id);
		
	}

	@Override
	public Optional<Order> findById(int id) {
		return orderRepository.findById(id);
	}

	@Override
	public List<Order> findOrdersByUserId(int UserId) {
		return orderRepository.findByUserId(UserId);
	}

	@Override
	public Order findOrdersByUserIdAndId(int id, int userId) {
		return orderRepository.findByIdAndUserId(id, userId);
	}

}
