package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.model.OrderDetail;
import com.fpoly.model.key.OrderDetailKey;
import com.fpoly.repository.OrderDetailRepository;
import com.fpoly.service.OrderDetailService;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {
	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@Override
	public List<OrderDetail> findAllOrderDetails() {
		return orderDetailRepository.findAll();
	}

	@Override
	public Optional<OrderDetail> findById(OrderDetailKey orderDetailKey) {
		return orderDetailRepository.findById(orderDetailKey);
	}

	@Override
	public OrderDetail saveOrderDetail(OrderDetail orderDetail) {
		return orderDetailRepository.save(orderDetail);
	}

	@Override
	public void deleteOrderDetailById(OrderDetailKey orderDetailKey) {
		orderDetailRepository.deleteById(orderDetailKey);
	}

	@Override
	public void deleteOrderDetailByOrderId(int id) {
		orderDetailRepository.deleteByOrderDetailKeyOrderId(id);
	}

	@Override
	public List<OrderDetail> findByOrderId(int id) {
		return orderDetailRepository.findByOrderDetailKeyOrderId(id);
	}



}
