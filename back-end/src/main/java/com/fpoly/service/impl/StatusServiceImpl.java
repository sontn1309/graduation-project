package com.fpoly.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fpoly.repository.StatusRepository;
import com.fpoly.service.StatusService;
import com.fpoly.model.Status;

@Service
public class StatusServiceImpl implements StatusService {

	@Autowired
	private StatusRepository statusRepository;

	@Override
	public List<Status> Status() {
		return statusRepository.findAll();
	}

	@Override
	public Optional<Status> findById(int id) {
		return statusRepository.findById(id);
	}

	@Override
	public Status saveStatus(Status status) {
		return statusRepository.save(status);
	}

	@Override
	public void deleteStatus(int id) {
		statusRepository.deleteById(id);
	}

}
