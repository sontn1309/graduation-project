package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Product;

public interface ProductService  {
	List<Product> findAllProducts();

	Optional<Product> findById(int id);

	Product saveProduct(Product product);

	void deleteProduct(int id);
}
