package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.OrderDetail;
import com.fpoly.model.key.OrderDetailKey;

public interface OrderDetailService {
	List<OrderDetail> findAllOrderDetails();

	Optional<OrderDetail> findById(OrderDetailKey orderDetailKey);

	List<OrderDetail> findByOrderId(int id);
	
	OrderDetail saveOrderDetail(OrderDetail orderDetail);

	void deleteOrderDetailById(OrderDetailKey orderDetailKey);
	
	void deleteOrderDetailByOrderId(int id);
}
