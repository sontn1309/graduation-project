package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.Repo;
import com.fpoly.model.key.RepoKey;

public interface RepoService {
	List<Repo> findAllRepos();

	Optional<Repo> findById(RepoKey repoKey);

	Repo saveRepo(Repo repo);

	void deleteRepo(RepoKey repoKey);
	
	void deletRepoByProductId(int productId);
	
	List<Repo> findByProductId(int productId);
}
