package com.fpoly.service;

import java.util.List;
import java.util.Optional;

import com.fpoly.model.UserApplication;

public interface UserService {
	Optional<UserApplication> getUser(String id);

	List<UserApplication> getAllUsers();

	UserApplication saveUser(UserApplication user);

	void deleteUser(String id);

	Optional<UserApplication> getUserByPhone(String phoneNumber);
}