package com.fpoly.jwt;

public class AuthenticationResponse {
    private int status;
    private String jwt;

    public AuthenticationResponse(int status, String jwt) {
        this.jwt = jwt;
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getJwt() {
        return this.jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

}