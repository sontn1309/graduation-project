package com.fpoly.repository;

import com.fpoly.model.Order;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Integer> {
	List<Order> findByUserId(int id);
	
	Order findByIdAndUserId(int id,int userId);
	
}
