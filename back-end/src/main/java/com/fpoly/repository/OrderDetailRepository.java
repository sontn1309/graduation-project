package com.fpoly.repository;

import com.fpoly.model.OrderDetail;
import com.fpoly.model.key.OrderDetailKey;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailRepository extends JpaRepository<OrderDetail,OrderDetailKey> {
	
	List<OrderDetail> findByOrderDetailKeyOrderId(int id);
	
	void deleteByOrderDetailKeyProductId(int id);
	
	void deleteByOrderDetailKeyOrderId(int id);
}
