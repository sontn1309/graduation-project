package com.fpoly.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fpoly.model.Status;

public interface StatusRepository extends JpaRepository<Status, Integer>{

}
