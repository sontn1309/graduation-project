package com.fpoly.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fpoly.model.Repo;
import com.fpoly.model.key.RepoKey;

public interface RepoRepository extends JpaRepository<Repo, RepoKey>{
	
	List<Repo>	findByRepoKeyProductId(int id);
	
	void 	deleteByRepoKeyProductId(int id);
}
