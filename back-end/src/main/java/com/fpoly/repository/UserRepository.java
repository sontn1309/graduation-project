package com.fpoly.repository;

import java.util.List;

import com.fpoly.model.UserApplication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserApplication, Integer> {
    List<UserApplication> findByPhone(String phoneNumber);
}