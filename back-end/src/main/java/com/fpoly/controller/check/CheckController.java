package com.fpoly.controller.check;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.exception.response.ResponseException;
import com.fpoly.model.UserApplication;
import com.fpoly.service.UserService;
import com.fpoly.utility.RequestConvertUtil;

@RestController
@CrossOrigin
@RequestMapping("/check")
public class CheckController {
	
	private UserService userService;
	
	public CheckController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("/role")
	public ResponseEntity<?> getMe(HttpServletRequest request) throws Exception {
		try {
			String phoneNumber = RequestConvertUtil.generatePhoneNumber(request);
			Optional<UserApplication> theUser = userService.getUserByPhone(phoneNumber);
			if (!theUser.isPresent()) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new ResponseException(404, "Not found", "Số điện thoại không tồn tại trên hệ thống"));
			}
			return ResponseEntity.ok(theUser.get().getRoles());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}

	}
}
