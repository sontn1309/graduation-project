package com.fpoly.controller.admin;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.dto.UserApplicationDTO;
import com.fpoly.dto.convert.ConvertUserApplicationDTO;
import com.fpoly.exception.response.ResponseException;
import com.fpoly.jwt.AuthenticationResponse;
import com.fpoly.jwt.JwtUtil;
import com.fpoly.model.Role;
import com.fpoly.model.UserApplication;
import com.fpoly.notification.ResponseNotification;
import com.fpoly.repository.RoleRepository;
import com.fpoly.service.MyUserDetails;
import com.fpoly.service.UserService;
import com.fpoly.sms.SmsSender;
import com.fpoly.utility.ConverterUtils;
import com.fpoly.utility.NumberUtils;
import com.fpoly.utility.PatternUtils;
import com.fpoly.utility.RequestConvertUtil;


@RestController
@RequestMapping("/admin/user")
@CrossOrigin
public class UserAdminController {
	private  UserService userService;

	private  PasswordEncoder bCryptPasswordEncoder;

	private  MyUserDetails myUserDetails;

	private  JwtUtil jwtTokenUtil;

	private RoleRepository roleRepository;

	private SmsSender smsSender;

	private Pattern patternPhone = Pattern.compile(PatternUtils.PATTERN_PHONE);
	
	private Pattern patternMail = Pattern.compile(PatternUtils.EMAIL_VERIFICATION);



	public UserAdminController(UserService userService, PasswordEncoder bCryptPasswordEncoder,
			MyUserDetails myUserDetails, JwtUtil jwtTokenUtil, RoleRepository roleRepository, SmsSender smsSender) {
		this.userService = userService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.myUserDetails = myUserDetails;
		this.jwtTokenUtil = jwtTokenUtil;
		this.roleRepository = roleRepository;
		this.smsSender = smsSender;
	}

	@GetMapping
	public ResponseEntity<?> get() throws Exception {
		try {
			return ResponseEntity.ok(userService.getAllUsers());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}

	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody UserApplicationDTO user) throws Exception {
		Matcher matcher = patternPhone.matcher(user.getPhone());
		if (!matcher.matches()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(403, "Access forbiden", "Số điện thoại không đúng"));
		}
		Optional<UserApplication> theTemp = userService.getUserByPhone(user.getPhone());
		if (!theTemp.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body(new ResponseException(404, "Not found", "Số điện thoại không tồn tại"));
		}

		if (!myUserDetails.checkUser(user.getPhone(), user)) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new ResponseException(403, "Acess forbiden", "Password không đúng"));
		}

		UserDetails userDetail = myUserDetails.loadUserByUsername(user.getPhone());
		String jwt = jwtTokenUtil.generateToken(userDetail);
		userService.saveUser(theTemp.get());
		return ResponseEntity.ok(new AuthenticationResponse(200, jwt));
	}

	@PostMapping("/forgot-password")
	public ResponseEntity<?> forgotPassword(@RequestBody UserApplicationDTO user) {
		String phoneNumber = ConverterUtils.convertPhoneNumber(user.getPhone());
		Optional<UserApplication> theTemp = userService.getUserByPhone(phoneNumber);
		Matcher matcher = patternPhone.matcher(phoneNumber);
		if (!matcher.matches()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(403, "Access forbiden", "Số điện thoại không đúng"));
		}
		if (!theTemp.isPresent()) {

			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.body(new ResponseException(404, "Not found", "Số điện thoại chưa được đăng kí"));
		}
		String password = NumberUtils.generateRandomString(6, "UPPER");
		String passwordBCrypt = bCryptPasswordEncoder.encode(password);
		theTemp.get().setPassword(passwordBCrypt).setPhone(phoneNumber).setConfirm(true);
		;
		try {
			userService.saveUser(theTemp.get());
			smsSender.sendSms(phoneNumber, "Mật khẩu mới của bạn là " + password);
			return ResponseEntity.ok(new ResponseNotification(200, "Mật khẩu đã được gửi vào số điện thoại của bạn"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Server Internal", "Lỗi hệ thống"));
		}
	}

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@RequestBody UserApplicationDTO user) {
		try {
			if (user.getPhone().isEmpty()||user.getEmail().isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại và Email không được để trống"));
			}
			String phoneNumber = ConverterUtils.convertPhoneNumber(user.getPhone());
			String passwordUncript = NumberUtils.generateRandomString(6, "UPPER");
			Optional<Role> role = roleRepository.findById(1);
			Set<Role> roles = new HashSet<Role>();
			roles.add(role.get());
			UserApplication theUser = new UserApplication();
			Matcher matcherPhone = patternPhone.matcher(phoneNumber);
			Matcher matcherMail = patternMail.matcher(user.getEmail());
			if (!matcherPhone.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại không đúng"));
			}
			if (!matcherMail.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Không đúng định dạng email"));
			}
			if (userService.getUserByPhone(user.getPhone()).isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Số điện thoại đã tồn tại"));
			}
			String password = bCryptPasswordEncoder.encode(passwordUncript);
			user.setPassword(password).setPhone(phoneNumber);
			theUser = ConvertUserApplicationDTO.conveDTOToUserApplication(user, roles);
			smsSender.sendSms(phoneNumber, "Bạn vừa đăng kí tài khoản tại TomShop với mật khẩu là " + passwordUncript
					+ ", chúc bạn có 1 ngày mua sắm thật vui vẻ");
			userService.saveUser(theUser);
			return ResponseEntity.ok(new ResponseNotification(200, "Bạn đã tạo tài khoản thành công"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}

	}

	@PostMapping("/regis-staff")
	public ResponseEntity<?> registerStaff(@RequestBody UserApplicationDTO user) {
		try {
			if (user.getPhone().isEmpty()||user.getEmail().isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại và Email không được để trống"));
			}
			String phoneNumber = ConverterUtils.convertPhoneNumber(user.getPhone());
			String passwordUncript = user.getPassword();
			Optional<Role> role = roleRepository.findById(2);
			Set<Role> roles = new HashSet<Role>();
			roles.add(role.get());
			UserApplication theUser = new UserApplication();
			Matcher matcherPhone = patternPhone.matcher(phoneNumber);
			Matcher matcherMail = patternMail.matcher(user.getEmail());
			if (!matcherPhone.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại không đúng"));
			}
			if (!matcherMail.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Không đúng định dạng email"));
			}
			if (userService.getUserByPhone(user.getPhone()).isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Số điện thoại đã tồn tại"));
			}
			String password = bCryptPasswordEncoder.encode(passwordUncript);
			user.setPassword(password).setPhone(phoneNumber);
			theUser = ConvertUserApplicationDTO.conveDTOToUserApplication(user, roles);
			userService.saveUser(theUser);
			return ResponseEntity.ok(new ResponseNotification(200, "Bạn đã tạo tài khoản nhân viên thành công"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}

	}
	
	@PostMapping("/regis-shipper")
	public ResponseEntity<?> registerShiper(@RequestBody UserApplicationDTO user) {
		try {
			if (user.getPhone().isEmpty()||user.getEmail().isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại và Email không được để trống"));
			}
			String phoneNumber = ConverterUtils.convertPhoneNumber(user.getPhone());
			String passwordUncript = user.getPassword();
			Optional<Role> role = roleRepository.findById(3);
			Set<Role> roles = new HashSet<Role>();
			roles.add(role.get());
			UserApplication theUser = new UserApplication();
			Matcher matcherPhone = patternPhone.matcher(phoneNumber);
			Matcher matcherMail = patternMail.matcher(user.getEmail());
			if (!matcherPhone.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Bad Request", "Số điện thoại không đúng"));
			}
			if (!matcherMail.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Không đúng định dạng email"));
			}
			if (userService.getUserByPhone(user.getPhone()).isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(400, "Access forbiden", "Số điện thoại đã tồn tại"));
			}
			String password = bCryptPasswordEncoder.encode(passwordUncript);
			user.setPassword(password).setPhone(phoneNumber);
			theUser = ConvertUserApplicationDTO.conveDTOToUserApplication(user, roles);
			userService.saveUser(theUser);
			return ResponseEntity.ok(new ResponseNotification(200, "Bạn đã tạo tài khoản shipper thành công"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}

	}
	@DeleteMapping
	public ResponseEntity<?> deleteUser(@RequestBody UserApplicationDTO user) {
		try {
			String phoneNumber = ConverterUtils.convertPhoneNumber(user.getPhone());
			Matcher matcher = patternPhone.matcher(phoneNumber);
			Optional<UserApplication> theUser = userService.getUserByPhone(user.getPhone());
			if (!matcher.matches()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(403, "Access forbiden", "Số điện thoại không đúng"));
			}
			if (!theUser.isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(new ResponseException(403, "Access forbiden", "Số điện thoại không tồn tại"));
			}

			return ResponseEntity.ok(new ResponseNotification(200, "Số điện thoại đã được xóa thành công"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Access forbiden", "Lỗi hệ thống"));
		}

	}

	@PutMapping
	public ResponseEntity<?> updateUser(@RequestBody UserApplicationDTO user, HttpServletRequest request) {
		try {
			String phoneNumber = RequestConvertUtil.generatePhoneNumber(request);
			Optional<UserApplication> theUser = userService.getUserByPhone(phoneNumber);
			if (!theUser.isPresent()) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new ResponseException(404, "Not found", "Số điện thoại không tồn tại trên hệ thống"));
			}
			return ResponseEntity.ok(new ResponseNotification(200, "Update thành công"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Access forbiden", "Lỗi hệ thống"));
		}
	}
}
