package com.fpoly.controller.admin;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.exception.response.ResponseException;
import com.fpoly.service.ProductService;

@RestController
@RequestMapping("/admin/products")
public class ProductAdminController {

	private ProductService productService;

	public ProductAdminController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping
	public ResponseEntity<?> getAllProducts() throws Exception {
		try {
			return ResponseEntity.ok(productService.findAllProducts());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}

	@GetMapping("/detail/{id}")
	public ResponseEntity<?> getProduct(@PathVariable(name = "id") String id) throws Exception {
		try {
			int productId=Integer.parseInt(id);
			return ResponseEntity.ok(productService.findById(productId));
		} catch (NumberFormatException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(400, "Bad request", "Sai định dạng của mã đơn hàng"));
		}
		catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
}
