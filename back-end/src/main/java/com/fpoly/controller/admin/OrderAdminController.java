package com.fpoly.controller.admin;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.exception.response.ResponseException;
import com.fpoly.service.OrderDetailService;
import com.fpoly.service.OrderService;

@RestController
@RequestMapping("/admin/order")
public class OrderAdminController {

	private OrderService orderService;
	
	private OrderDetailService orderDetailService;

	
	public OrderAdminController(OrderService orderService, OrderDetailService orderDetailService) {
		this.orderService = orderService;
		this.orderDetailService = orderDetailService;
	}

	@GetMapping
	public ResponseEntity<?> getAllOrders() throws Exception {
		try {
			return ResponseEntity.ok(orderService.findAllOrders());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}

	@GetMapping("/detail/{id}")
	public ResponseEntity<?> getOrder(@PathVariable(name = "id") String id) throws Exception {
		try {
			int orderId=Integer.parseInt(id);
			return ResponseEntity.ok(orderDetailService.findByOrderId(orderId));
		} catch (NumberFormatException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
		catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
}
