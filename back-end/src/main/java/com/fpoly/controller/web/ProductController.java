package com.fpoly.controller.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.exception.response.ResponseException;
import com.fpoly.notification.SendTelegram;
import com.fpoly.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	private ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping
	public ResponseEntity<?> getAllProducts() throws Exception {
		try {
			SendTelegram x= new SendTelegram();
			x.sendTelegram();
			return ResponseEntity.ok(productService.findAllProducts());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> getProductById(@PathVariable("id") String id) throws Exception {
		try {
			return ResponseEntity.ok(productService.findById(Integer.parseInt(id)));
		}catch (NumberFormatException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(400, "Bad Request", "Không tìm thấy sản phẩm"));
		}
		catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
	

}
