package com.fpoly.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpoly.exception.response.ResponseException;
import com.fpoly.model.Order;
import com.fpoly.model.UserApplication;
import com.fpoly.service.OrderDetailService;
import com.fpoly.service.OrderService;
import com.fpoly.service.UserService;
import com.fpoly.utility.RequestConvertUtil;

@RestController
@RequestMapping("/order")
public class OrderController {

	private OrderService orderService;

	private OrderDetailService orderDetailService;

	private UserService userService;

	public OrderController(OrderService orderService, OrderDetailService orderDetailService) {
		this.orderService = orderService;
		this.orderDetailService = orderDetailService;
	}

	@GetMapping
	public ResponseEntity<?> getAllOrders(HttpServletRequest request) throws Exception {
		try {
			String phoneNumber = RequestConvertUtil.generatePhoneNumber(request);
			UserApplication theUser = userService.getUserByPhone(phoneNumber).get();

			return ResponseEntity.ok(orderService.findOrdersByUserId(theUser.getId()));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}

	@GetMapping("/detail/{id}")
	public ResponseEntity<?> getOrder(@PathVariable(name = "id") String id, HttpServletRequest request)
			throws Exception {
		try {
			int orderId = Integer.parseInt(id);
			String phoneNumber = RequestConvertUtil.generatePhoneNumber(request);
			UserApplication theUser = userService.getUserByPhone(phoneNumber).get();
			int userId = theUser.getId();
			Order order = orderService.findOrdersByUserIdAndId(orderId, userId);
			return ResponseEntity.ok(orderDetailService.findByOrderId(order.getId()));
		} catch (NumberFormatException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(400, "Bad request", "Không tìm thấy đơn hàng yêu cầu"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
	
	@PutMapping
	public ResponseEntity<?> updateStatus(@PathVariable(name = "id") String id, HttpServletRequest request)
			throws Exception {
		try {
			int orderId = Integer.parseInt(id);
			String phoneNumber = RequestConvertUtil.generatePhoneNumber(request);
			UserApplication theUser = userService.getUserByPhone(phoneNumber).get();
			int userId = theUser.getId();
			Order order = orderService.findOrdersByUserIdAndId(orderId, userId);
			return ResponseEntity.ok(orderDetailService.findByOrderId(order.getId()));
		} catch (NumberFormatException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new ResponseException(400, "Bad request", "Không tìm thấy đơn hàng yêu cầu"));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseException(500, "Serve internal", "Lỗi hệ thống"));
		}
	}
}
