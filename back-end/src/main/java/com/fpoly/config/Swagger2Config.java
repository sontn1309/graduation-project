package com.fpoly.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config {
	private static final Set<String> DEFAULT_PRODUCES_CONSUMES = new HashSet<String>(Arrays.asList("application/json"));
    
    @Bean
    public Docket api() {
      ParameterBuilder parameterBuilder = new ParameterBuilder();
      parameterBuilder.name("Authorization")
              .modelRef(new ModelRef("string"))
              .parameterType("header")
              .description("JWT token")
              .required(false)
              .build();
      List<Parameter> parameters = new ArrayList<>();
      parameters.add(parameterBuilder.build());
      return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
          .produces(DEFAULT_PRODUCES_CONSUMES)
          .consumes(DEFAULT_PRODUCES_CONSUMES)
          .select()
          .build()
          .globalOperationParameters(parameters);
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Tom Shop API")
                                   .description("API for fashion shop online")
                                   .contact(new Contact("Ngọc Sơn", "https://www.facebook.com/ngocson454/", "sontn1309@gmail.com"))
                                   .license("Author : sontn")
                                   .licenseUrl("https://www.facebook.com/ngocson454/")
                                   .version("1.0.0")
                                   .build();
    }
  }
    
