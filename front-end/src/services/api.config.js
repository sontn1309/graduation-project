import axios from 'axios'
import { TokenService } from './storage.service'

var url ='http://localhost:8080/';
const ApiService = {

    init(baseURL) {
        axios.defaults.baseURL = baseURL;
    },

    setHeader() {
        axios.defaults.headers.common["Authorization"] = `Bearer ${TokenService.getToken()}`
    },

    removeHeader() {
        axios.defaults.headers.common = {}
    },

    get(resource) {
        return axios.get(url+resource)
    },

    post(resource, data) {
        return axios.post(url+resource, data)
    },

    put(resource, data) {
        return axios.put(url+resource, data)
    },

    delete(resource) {
        return axios.delete(url+resource)
    },

    customRequest(data) {
        return axios(data)
    }
}

export default ApiService;