import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './login.css';
import Header from '../../components/header/header.component'
import swal from 'sweetalert';
import ApiService from '../../services/api.config';
export default class SignUp extends Component {
    constructor(props) { 
        super(props);
        this.state = {          
                address: "",
                email: "",
                name: "",
                phone: ""
              
        };
        this.handleChangePhone = this.handleChangePhone.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChangePhone(event) {
        this.setState({phone: event.target.value});
      } 
      handleChangeEmail(event) {
        this.setState({email: event.target.value});
      } 
      handleChangeName(event) {
        this.setState({name: event.target.value});
      } 
      handleChangeAddress(event) {
        this.setState({address: event.target.value});
      } 
      handleSubmit(event) {
        event.preventDefault();
        var user = {'phone': this.state.phone,
                    'email':this.state.email,
                     'name':this.state.name   ,
                    'address':this.state.address};
        ApiService.post("user/register",user).then(response => {
            swal("Thành công!",'Đăng kí thành công! Mật khẩu sẽ được gửi vào số điện thoại của bạn', "success").then(
                ()=> {window.location.href ="/sign-in";}
            );
        })
        .catch(e=> {swal("Lỗi!",e.response.data.message , "error")})
      }
    render() {
        return (
            <div className="App mb-5">
                    <div className="auth-wrapper">
                        <div className="auth-inner">
                   
            <form onSubmit={this.handleSubmit}>
                <h3>Tạo tài khoản</h3>
                <div className="form-group">
                    <label>Biệt danh của bạn</label>
                    <input type="text" className="form-control"  value={this.state.name}  onChange={this.handleChangeName}/>
                </div>
                <div className="form-group">
                    <label>Số điện thoại</label>
                    <input type="text" className="form-control" value={this.state.phone}  onChange={this.handleChangePhone}/>
                </div>

                <div className="form-group">
                    <label>Email </label>
                    <input type="email" className="form-control" value={this.state.email}  onChange={this.handleChangeEmail}/>
                </div>
                <div className="form-group">
                    <label>Địa chỉ</label>
                    <input type="text" className="form-control" value={this.state.address}  onChange={this.handleChangeAddress}/>
                </div>
                <button type="submit" className="btn btn-primary btn-block">Đăng kí </button>
                <p className="forgot-password text-right">
                   Bạn đã có tài khoản <Link className="link" to={"/sign-in"}>Đăng nhập</Link>
                </p>
            </form>
            </div>
                </div>
            </div>
        );
    }
}