import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './login.css';
import swal from 'sweetalert';
import ApiService from '../../services/api.config'
export default class ForgotPassword extends Component {
    constructor(props) { 
        super(props);
        this.state = {
          phone: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChange(event) {
        this.setState({phone: event.target.value});
      }
      handleSubmit(event) {
        event.preventDefault();
        var phone = {'phone': this.state.phone};
        ApiService.post("user/forgot-password",phone).then(response => {
            swal("Thành công!",response.data.message , "success").then(
                ()=> {window.location.href ="/sign-in";}
            );
        })
        .catch(e=> {swal("Lỗi!",e.response.data.message , "error")})
      }
    render() {
        return (
            <div className="App mb-5">
                 <div className="auth-wrapper">
                    <div className="auth-inner">
                    <form onSubmit={this.handleSubmit}>
                        <h3>Quên mật khẩu</h3>
                        <div className="form-group">
                            <label>Nhập số điện thoại</label>
                            <input type="text" className="form-control" value={this.state.phone}  onChange={this.handleChange}/>
                        </div>       
                        <button type="submit" className="btn btn-primary btn-block">Lấy lại mật khẩu </button>
                        <p className="forgot-password text-right">
                        Bạn đã có tài khoản <Link className="link" to={"/sign-in"}>Đăng nhập</Link>
                        </p>
                    </form>
                    </div>
                </div>
            </div>
        );
    }
}