import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './login.css';
import swal from 'sweetalert';
import ApiService from '../../services/api.config'
import  { TokenService, SetUser } from '../../services/storage.service'
export default class Login extends Component {
    constructor(props) { 
        super(props);
        this.state = {
          phone: "",
          password: ""
        };
        this.handleChangePhone = this.handleChangePhone.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleChangePhone(event) {
        this.setState({phone: event.target.value});
      } 
      handleChangePass(event) {
        this.setState({password: event.target.value});
      } 
      handleSubmit(event) {
            event.preventDefault();
            var user = {'phone': this.state.phone,
                        'password':this.state.password};
            ApiService.post("user/login",user).then(response => {
                TokenService.saveToken(response.data.jwt);
                swal("Thành công!",'Đăng nhập thành công!', "success").then(
                    ()=> {window.location.href ="/";}
                );
            })
            .catch(e=> {
                swal("Lỗi!",e.response.data.message , "error");  
            });   
      }
    render() {
        return (
                <div className="App mb-5">
                        <div className="auth-wrapper">
                        <div className="auth-inner">
                        <form onSubmit={this.handleSubmit}>
                            <h3>Đăng nhập</h3>
                            <div className="form-group">
                                <label>Số điện thoại</label>
                                <input type="text" className="form-control" value={this.state.phone}  onChange={this.handleChangePhone}/>
                            </div>

                            <div className="form-group">
                                <label>Mật khẩu</label>
                                <input type="password" className="form-control" value={this.state.password}  onChange={this.handleChangePass}/>
                            </div>

                            <div className="form-group">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                    <label className="custom-control-label" htmlFor="customCheck1">Ghi nhớ đăng nhập</label>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary btn-block">Đăng nhập</button>
                            <div className="row">
                            <div className="col-sm"> <p className="forgot-password">
                                <Link className="link" to={"/sign-up"}>Đăng kí</Link> 
                            </p></div>
                            <div className="col-sm"> <p className="forgot-password text-right">                  
                               <Link className="link" to={"/forgot-password"}>Quên mật khẩu</Link>     
                            </p></div>
                            </div>                          
                    </form>
                    </div>
                </div>
            </div>
        );
    }
}