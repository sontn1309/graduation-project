import React, { useContext ,useState} from 'react';
import ProductItem from './ProductItem';
import { ProductsContext } from '../../contexts/ProductsContext';
import styles from './ProductsGrid.module.scss';
import { dummyProducts } from '../../services/dummy';

const ProductsGrid = () => {

    const [products] = useState(dummyProducts);

    return ( 
        <div className={styles.p__container}>
            <div className="row">        
            </div>
            <div className={styles.p__grid}>

                {
                    products.map(product => (
                        <ProductItem key={product.id} product={product}/>
                    ))
                }

            </div>
            <div className={styles.p__footer}>

            </div>
        </div>
     );
}
 
export default ProductsGrid;