import React, { Component } from 'react';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import IconFacebook from '../../logo/iconf.png'
import IconInstagram from '../../logo/iconi.png'
export default class Footer extends Component{
  render (){
  return (
       <footer className="page-footer font-small pt-4  text-white bg-dark clearfix">
        <div className="row ml-5">
          <div className="col-sm-4">
            <p align="left">
              HỒ CHÍ MINH <br /><br />
              STORE 1 <br />
              17bis Huỳnh Đình Hai Phường 14 - Quận Bình Thạnh <br />
              02866 506 201 <br /><br />
              STORE 2 <br />
              332 Cao Đạt Phường 1 - Quận 5 <br />
              02866 506 202 <br /><br />
              STORE 3 <br />
              188/16 Thành Thái Phường 12 Quận 10<br />
              02866 506 203<br />
            </p>
          </div>
          <div className="col-sm-4">
            <p align="left">
              <br /><br />
              STORE 4<br />
              1002 Quang Trung, Phường 8, Quận Gò Vấp<br />
              02866 506 204<br /><br />
              STORE 5<br />
              164-166 Trường Chinh, Phường 13, Quận Tân Bình<br /><br />
              028 66506205
            </p>
          </div>
          <div className="col-sm-4">
            <p align="left">
              HÀ NỘI <br /><br />
              STORE 6<br />
              79 Nguyễn Khang, Trung Hòa, Cầu Giấy<br />
              02466 562 205<br /><br />
              STORE 8<br />
              43 Khâm Thiên - Quận Đống Đa <br />
              02466 562 210<br /><br />
              BIÊN HÒA<br /><br />
              STORE 7 <br />
              237, Trương Định, Biên Hòa <br />
              0961 789 207
            </p>
          </div>
        </div>
        <hr color="white" />
        <div className="row ml-5">
          <div className="col-sm-4">
            <p>
              MẠNG XÃ HỘI <br />
              <img src={IconFacebook} width="20px" className="mr-2"/> 
              <img src={IconInstagram} width="25px" />
            </p>
          </div>
          <div className="col-sm-4">
          </div>
          <div className="col-sm-4">
            <p align="left">
              CHÍNH SÁCH <br />
              Hướng dẫn đặt hàng<br />
              Chính sách đổi trả <br />
              Kiêm tra đơn hàng
            </p>
          </div>
        </div>
        <hr color="white" />
        <div className="col-sm-12 mt-3">© 2020 Copyright: TOM STORE
        </div>
        <hr />
    </footer>
    )
  };
};
