import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import logo from '../../logo/logo_transparent.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser,faSearch,faShoppingCart } from '@fortawesome/free-solid-svg-icons'

export default class Header extends Component {
    render() {
        return (   
          <div>   
        <nav className="navbar navbar-expand navbar-dark bg-dark flex-column flex-md-row bd-navbar shadow-lg fixed-top rounded">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse ml-5" id="navbarTogglerDemo01">
          <Link className="navbar-brand " to={"/"}><img src={logo} height= "40px" width="40px" /></Link>
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item active">
              <Link className="nav-link" to={"/sign-up"}>Trang chủ</Link>
              </li> 
              <li className="nav-item active">
              <Link className="nav-link" to={"/forgot-password"}>Quên mật khẩu </Link>
              </li>
              <li className="nav-item dropdown active ">
              <Link className="nav-link dropdown-toggle" to={"/forgot-password"} id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sản phẩm
                </Link>
            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <Link className="dropdown-item" to={"/forgot-password"}>Áo Nam</Link>
            <Link className="dropdown-item" to={"/forgot-password"}>Áo Nữ</Link>
            <Link className="dropdown-item" to={"/forgot-password"}>Quần nam</Link>
            <Link className="dropdown-item" to={"/forgot-password"}>Quần nữ</Link>
            </div>
            </li>
            <li className="nav-item active">
            <Link className="nav-link" to={"/forgot-password"}>Liên hệ <span className="sr-only">(current)</span></Link>
              </li>
              <li className="nav-item active">
              <Link className="nav-link" to={"/forgot-password"}>Góp ý <span className="sr-only">(current)</span></Link>
              </li>
            </ul>
            <ul className="navbar-nav ml-4 mr-5 mt-lg-0">
              <li  className=" ml-3 text-white"><FontAwesomeIcon icon={faUser}/></li>
              <li  className=" ml-3 text-white"><FontAwesomeIcon icon={faShoppingCart}/></li>
              <li   className=" ml-3 text-white"> <FontAwesomeIcon icon={faSearch}  /></li>
            </ul>
          </div>
        </nav>
         <nav className="navbar navbar-expand navbar-dark bg-dark flex-column flex-md-row bd-navbar shadow-lg  rounded">
              <Link className="navbar-brand " to={"/"}><img src={logo} height= "40px" width="40px" /></Link>
        </nav>
       </div> 
        );
    }
}