import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from './pages/home/HomePage'
import Login from './pages/login/login.component'
import SignUp from './pages/login/signup.component'
import ForgotPassword from './pages/login/forgot.component'
import Header from './components/header/header.component';

import Footer from './components/footer/footer.component';

import Store from './pages/store/index';


function App() {
  return (
        <div>
          <Router>
            <div ><Header/></div>
              <div className="container">
                  <Switch>
                    <Route exact path='/' component={Store} />
                    <Route  path='/sign-in' component={Login} />
                    <Route  path='/sign-up' component={SignUp} />
                    <Route  path='/forgot-password' component={ForgotPassword} />
                  </Switch>
              </div>
              <div className="mr-auto footer mt-5"> <Footer /></div>
          </Router>
         
      </div>
  );
}

export default App;